﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MeetUp.Controllers
{
    using DAL;
    using Models;
    using Models.ViewModels.Course;

    public class CourseController : Controller
    {

        private IDataService data;



        // Constructors
        // ============

        public CourseController() : this(new DataService()) { }

        public CourseController(IDataService data)
        {
            this.data = data;
        }



        // Action methods
        // ==============

        // GET: Course
        public ActionResult Index()
        {
            return HttpNotFound();
        }

        // GET: Course/Add?scheduleID={scheduleID}
        [HttpGet]
        public ActionResult Add(Guid scheduleID)
        {
            // look up schedule
            Schedule schedule = data.GetSchedule(scheduleID);
            if (schedule == null)
                return HttpNotFound();
            Guid userID = Guid.Parse(User.Identity.GetUserId());
            if (schedule.UserID != userID)
                return HttpNotFound();

            // look up courses
            // TEMPORARY: ignore institution
            IEnumerable<Course> courses = data.GetCourses(Guid.Empty);

            return View(new AddCourseViewModel
            {
                Schedule = schedule,
                Courses = courses
            });
        }

        // POST: Course/Add?scheduleID={scheduleID}
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Add(Guid scheduleID, AddCourseViewModel viewModel)
        {
            // look up schedule
            Schedule schedule = data.GetSchedule(scheduleID);
            if (schedule == null)
                return HttpNotFound();
            Guid userID = Guid.Parse(User.Identity.GetUserId());
            if (schedule.UserID != userID)
                return HttpNotFound();

            // look up course
            // TEMPORARY: ignore institution
            Course course = data.GetCourse(
                    Guid.Empty, viewModel.SubjectCode,
                    viewModel.CourseNumber, viewModel.CourseSection
                );
            if (course == null)
                ModelState.AddModelError(string.Empty, "Could not find course");
            else if (data.IsCourseInSchedule(course.UID, scheduleID))
                ModelState.AddModelError(string.Empty, "Course is already added to schedule");

            if (ModelState.IsValid)
            {
                // add course and return to edit schedule page
                data.AddCourseToSchedule(course.UID, scheduleID);
                return RedirectToAction("Edit", "Schedule", new { id = scheduleID });
            }
            else
            {
                // redisplay form
                viewModel.Schedule = schedule;
                viewModel.Courses = data.GetCourses(Guid.Empty); // TEMPORARY: ignore instituion
                return View(viewModel);
            }
        }

        // GET: Course/Remove?courseID={courseID}&scheduleID={scheduleID}
        [HttpGet]
        public ActionResult Remove(Guid courseID, Guid scheduleID)
        {
            // look up schedule and course
            Schedule schedule = data.GetSchedule(scheduleID);
            if (schedule == null)
                return HttpNotFound();
            Guid userID = Guid.Parse(User.Identity.GetUserId());
            if (schedule.UserID != userID)
                return HttpNotFound();
            Course course = data.GetCourse(courseID);
            if (course == null)
                return HttpNotFound();

            // build view model
            RemoveCourseViewModel viewModel = new RemoveCourseViewModel
            {
                Schedule = schedule,
                Course = course
            };

            return View(viewModel);
        }

        // POST: Course/Remove?courseID={courseID}&scheduleID={scheduleID}
        [HttpPost, ActionName("Remove"), ValidateAntiForgeryToken]
        public ActionResult RemoveConfirmed(Guid courseID, Guid scheduleID)
        {
            // look up schedule and course
            Schedule schedule = data.GetSchedule(scheduleID);
            if (schedule == null)
                return HttpNotFound();
            Guid userID = Guid.Parse(User.Identity.GetUserId());
            if (schedule.UserID != userID)
                return HttpNotFound();
            Course course = data.GetCourse(courseID);
            if (course == null)
                return HttpNotFound();

            // verify that course is in schedule
            if (!data.IsCourseInSchedule(courseID, scheduleID))
                return HttpNotFound();

            // remove course from schedule and return to edit schedule page
            data.RemoveCourseFromSchedule(courseID, scheduleID);
            return RedirectToAction("Edit", "Schedule", new { id = scheduleID });
        }



        // Disposal
        // ========

        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (disposing && !this.disposed)
            {
                this.data.Dispose();
                this.disposed = true;
            }
            base.Dispose(disposing);
        }

    }
}