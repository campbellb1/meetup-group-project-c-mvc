﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MeetUp.Controllers
{
    using DAL;

    public class SuggestionController : Controller
    {

        private IDataService data;



        // Constructors
        // ============

        public SuggestionController() : this(new DataService()) { }

        public SuggestionController(IDataService data)
        {
            this.data = data;
        }



        // Action methods
        // ==============

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Accept(Guid user2)
        {
            // look up user
            Guid userID = Guid.Parse(User.Identity.GetUserId());

            // do magical acceptance stuff
            var currentAcceptDenyCount = data.GetAcceptDenyCount(userID, user2);

            if (currentAcceptDenyCount == null)
            {
                data.InsertAcceptDenyRelation(userID, user2);
                currentAcceptDenyCount = data.GetAcceptDenyCount(userID, user2);
            }

            currentAcceptDenyCount.Accepted = true;

            data.SaveAcceptDenyCount(currentAcceptDenyCount);
            //throw new NotImplementedException();

            // return to user home page
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Deny(Guid user2)
        {
            // look up user
            Guid userID = Guid.Parse(User.Identity.GetUserId());

            //Get the Row for users
            var currentAcceptDenyCount = data.GetAcceptDenyCount(userID, user2);
            if (currentAcceptDenyCount == null)
            {
                data.InsertAcceptDenyRelation(userID, user2);
                currentAcceptDenyCount = data.GetAcceptDenyCount(userID, user2);
            }

            currentAcceptDenyCount.DenyCount = ++currentAcceptDenyCount.DenyCount;
            data.SaveAcceptDenyCount(currentAcceptDenyCount);

            // if the other user accepted, clear their acceptance
            var otherAdc = data.GetAcceptDenyCount(user2, userID);
            if (otherAdc != null && otherAdc.Accepted)
            {
                otherAdc.Accepted = false;
                data.SaveAcceptDenyCount(otherAdc);
            }

            // return to user home page
            return RedirectToAction("Index", "Home");
        }



        // Disposal
        // ========

        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (disposing && !this.disposed)
            {
                this.data.Dispose();
                this.disposed = true;
            }
            base.Dispose(disposing);
        }

    }
}
