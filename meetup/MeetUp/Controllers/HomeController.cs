﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MeetUp.Controllers
{
    using DAL;
    using Models;
    using Models.ViewModels.Home;

    public class HomeController : Controller
    {

        private IDataService data;



        // Constructors
        // ============

        public HomeController() : this(new DataService()) { }

        public HomeController(IDataService data)
        {
            this.data = data;
        }



        // Action methods
        // ==============

        public ActionResult Index()
        {
            if (!Request.IsAuthenticated)
                return View("Index");
            else
                return UserHome();
        }
        


        // Helper methods
        // ==============

        public ActionResult UserHome()
        {
            // look up user
            Guid userID = Guid.Parse(User.Identity.GetUserId());
            User user = data.EnsureUser(userID);

            // look up user's current schedule and schedules matching it
            Schedule schedule = data.GetUserCurrentSchedule(userID);
            Schedule[] matches = data.GetMatchingSchedules(schedule.ID).ToArray();
            Dictionary<Guid, double> compatibilityData = new Dictionary<Guid, double>();
            // look up other info on matching schedules
            Dictionary<Schedule, string> userNames = new Dictionary<Schedule, string>();
            Dictionary<Schedule, Course[]> matchingCourses = new Dictionary<Schedule, Course[]>();
            Dictionary<Schedule, double> compatibility = new Dictionary<Schedule, double>();
            Dictionary<Schedule, bool> acceptance = new Dictionary<Schedule, bool>();
            Dictionary<Schedule, bool> pending = new Dictionary<Schedule, bool>();
            foreach (Schedule s in matches)
            {
                userNames[s] = data.GetUserName(s.UserID);
                matchingCourses[s] = data.GetCommonCourses(schedule.ID, s.ID).ToArray();
                compatibility[s] = data.GetCompatibility(userID, s.UserID);
                acceptance[s] = data.GetAcceptance(userID, s.UserID);
                pending[s] = data.GetPending(userID, s.UserID);
            }

            matches = matches.Where(s => compatibility[s] > 0).ToArray();

            // construct view model and return view
            UserHomeViewModel viewModel = new UserHomeViewModel
            {
                CurrentSchedule = schedule,
                MatchingSchedules = matches,
                UserNames = userNames,
                MatchingCourses = matchingCourses,
                Compatibility = compatibility,
                Acceptance = acceptance,
                Pending = pending
            };
            return View("UserHome", viewModel);
        }



        // Disposal
        // ========

        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (disposing && !this.disposed)
            {
                this.data.Dispose();
                this.disposed = true;
            }
            base.Dispose(disposing);
        }

    }
}