﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;

namespace MeetUp.Controllers
{
    using DAL;
    using Models;
    using Models.ViewModels.Schedule;

    [Authorize]
    public class ScheduleController : Controller
    {

        private IDataService data;



        // Constructors
        // ============

        public ScheduleController() : this(new DataService()) { }

        public ScheduleController(IDataService data)
        {
            this.data = data;
        }



        // Action methods
        // ==============

        // GET: Schedule
        public ActionResult Index()
        {
            // TEMPORARY: just redirect to Edit page for current schedule

            // look up user
            Guid userID = Guid.Parse(User.Identity.GetUserId());
            User user = data.EnsureUser(userID);

            // look up user's current schedule
            Schedule schedule = data.GetUserCurrentSchedule(userID);

            // redirect to edit schedule page
            return RedirectToAction("Edit", new { id = schedule.ID });
        }

        // GET: Schedule/Edit/{id}
        public ActionResult Edit(Guid id)
        {
            // look up schedule
            Schedule schedule = data.GetSchedule(id);
            if (schedule == null)
                return HttpNotFound();
            Guid userID = Guid.Parse(User.Identity.GetUserId());
            if (schedule.UserID != userID)
                return HttpNotFound();

            // build view model
            EditScheduleViewModel viewModel = new EditScheduleViewModel
            {
                Schedule = schedule,
                Courses = data.GetCoursesForSchedule(id).ToArray()
                
            };

            return View(viewModel);
        }



        // Disposal
        // ========

        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (disposing && !this.disposed)
            {
                this.data.Dispose();
                this.disposed = true;
            }
            base.Dispose(disposing);
        }

    }
}