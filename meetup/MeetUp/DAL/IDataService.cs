﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetUp.DAL
{
    using Models;

    public interface IDataService : IDisposable
    {
        void AddCourseToSchedule(Guid courseID, Guid scheduleID);
        User EnsureUser(Guid userID);
        bool GetAcceptance(Guid user1, Guid user2);
        AcceptDenyCount GetAcceptDenyCount(Guid user1, Guid user2);
        IEnumerable<Course> GetCommonCourses(Guid schedule1, Guid schedule2);
        double GetCompatibility(Guid userID_1, Guid userID_2);
        Course GetCourse(Guid id);
        Course GetCourse(Guid institutionID, string subject, string course, string section);
        IEnumerable<Course> GetCourses(Guid institutionID);
        IEnumerable<Course> GetCoursesForSchedule(Guid scheduleID);
        IEnumerable<Schedule> GetMatchingSchedules(Guid scheduleID);
        bool GetPending(Guid user1, Guid user2);
        Schedule GetSchedule(Guid id);
        ApplicationUser GetUser(Guid userID);
        Schedule GetUserCurrentSchedule(Guid userID);
        string GetUserName(Guid userID);
        IEnumerable<Schedule> GetUserSchedules(Guid userID);
        void InsertAcceptDenyRelation(Guid userID_1, Guid userID_2);
        bool IsCourseInSchedule(Guid courseID, Guid scheduleID);
        void RemoveCourseFromSchedule(Guid courseID, Guid scheduleID);
        void SaveAcceptDenyCount(AcceptDenyCount acceptDenyCount);
        void SaveSchedule(Schedule schedule);
    }
}
