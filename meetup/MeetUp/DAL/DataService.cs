﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MeetUp.DAL
{
    using Models;

    public class DataService : IDataService
    {
        private AuthDbContext authDb;
        private Entities db;

        public DataService()
        {
            this.authDb = new AuthDbContext();
            this.db = new Entities();
        }

        public void AddCourseToSchedule(Guid courseID, Guid scheduleID)
        {
            if (db.Schedule_Courses.Count(sc => sc.CourseID == courseID && sc.ScheduleID == scheduleID) == 0)
            {
                db.Schedule_Courses.Add(new ScheduleCourse
                {
                    UID = Guid.NewGuid(),
                    CourseID = courseID,
                    ScheduleID = scheduleID
                });
                db.SaveChanges();
            }
        }

        public User EnsureUser(Guid id)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                user = new User
                {
                    UID = id,
                    FirstName = string.Empty,
                    LastName = string.Empty
                };
                db.Users.Add(user);
                db.SaveChanges();
            }
            return user;
        }

        public bool GetAcceptance(Guid user1, Guid user2)
        {
           bool User_1_Accepted = GetAcceptDenyCount(user1, user2)?.Accepted??false;
           bool User_2_Accepted = GetAcceptDenyCount(user2, user1)?.Accepted??false;
            if (User_1_Accepted && User_2_Accepted)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public AcceptDenyCount GetAcceptDenyCount(Guid user1, Guid user2)
        {
            return db.AcceptDeny_Count
                .Where(adc => adc.User1 == user1 && adc.User2 == user2)
                .FirstOrDefault();
        }

        public IEnumerable<Course> GetCommonCourses(Guid schedule1, Guid schedule2)
        {
            Course[] schedule1Courses = this.GetCoursesForSchedule(schedule1).ToArray();
            Course[] schedule2Courses = this.GetCoursesForSchedule(schedule2).ToArray();

            return schedule1Courses.Where(c1 => schedule2Courses.Count(c2 => c2.UID == c1.UID) > 0);
        }

        public double GetCompatibility(Guid User_1, Guid User_2)
        {
           Schedule User_1_Schedule = GetUserCurrentSchedule(User_1);
           Schedule User_2_Schedule = GetUserCurrentSchedule(User_2);
           int numCommonCourses = GetCommonCourses(User_1_Schedule.ID, User_2_Schedule.ID).Count();
           int numTotalCourses = User_1_Schedule.ScheduleCourses.Count();

            AcceptDenyCount denyCountRow = GetAcceptDenyCount(User_1, User_2);
            int denyCount = denyCountRow?.DenyCount??0;

           return (((double)numCommonCourses / numTotalCourses) - (denyCount * 0.20));
        }   


        public Course GetCourse(Guid id)
        {
            return db.Courses.Find(id);
        }

        public Course GetCourse(Guid institutionID, string subject, string course, string section)
        {
            var query = db.Courses
                .Where(c => c.SubjectCode == subject)
                .Where(c => c.CourseNumber == course)
                .Where(c => c.CourseSection == section);

            if (institutionID != Guid.Empty)
                query = query.Where(c => c.EductationalInstitutionID == institutionID);

            return query.FirstOrDefault();
        }

        public IEnumerable<Course> GetCourses(Guid institutionID)
        {
            var query = db.Courses.AsQueryable();

            if (institutionID != Guid.Empty)
                query = query.Where(c => c.EductationalInstitutionID == institutionID);

            return query;
        }

        public IEnumerable<Course> GetCoursesForSchedule(Guid scheduleID)
        {
            return db.Schedule_Courses
                .Where(sc => sc.ScheduleID == scheduleID)
                .Select(sc => sc.Courses);
        }

        public IEnumerable<Schedule> GetMatchingSchedules(Guid scheduleID)
        {
            Schedule schedule = this.GetSchedule(scheduleID);
            if (schedule == null)
                return new Schedule[0];

            return 
                db.Schedule_Courses
                .Where(sc => sc.ScheduleID == scheduleID)
                .SelectMany(sc1 =>
                    db.Schedule_Courses
                    .Where(sc2 => sc2.CourseID == sc1.CourseID)
                    .Where(sc2 => sc2.ScheduleID != scheduleID)
                    .Select(sc2 => sc2.ScheduleID)
                )
                .Distinct()
                .ToArray()
                .Select(id => this.GetSchedule(id));
        }

        public bool GetPending(Guid user1, Guid user2)
        {
            bool User_1_Accepted = GetAcceptDenyCount(user1, user2)?.Accepted ?? false;
            bool User_2_Accepted = GetAcceptDenyCount(user2, user1)?.Accepted ?? false;
            if (User_1_Accepted && !User_2_Accepted)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Schedule GetSchedule(Guid id)
        {
            return db.Schedule.Find(id);
        }

        public ApplicationUser GetUser(Guid userID)
        {
            return authDb.Users.Where(u => u.Id == userID.ToString()).FirstOrDefault();
        }

        public Schedule GetUserCurrentSchedule(Guid userID)
        {
            // TEMPORARY: user can only have one schedule, which is the current schedule

            if (db.Schedule.Count(s => s.UserID == userID) == 0)
                this.SaveSchedule(new Schedule { UserID = userID });

            return db.Schedule.Where(s => s.UserID == userID).First();
        }

        public string GetUserName(Guid userID)
        {
            return this.GetUser(userID)?.UserName;
        }

        public IEnumerable<Schedule> GetUserSchedules(Guid userID)
        {
            return db.Schedule.Where(s => s.UserID == userID);
        }

        public void InsertAcceptDenyRelation(Guid userID_1, Guid userID_2)
        {
            AcceptDenyCount newAcceptDenyCount = new AcceptDenyCount
            {
                UID = Guid.NewGuid(),
                User1 = userID_1,
                User2 = userID_2
            };
            db.AcceptDeny_Count.Add(newAcceptDenyCount);
            db.SaveChanges();
        }

        public bool IsCourseInSchedule(Guid courseID, Guid scheduleID)
        {
            return db.Schedule_Courses
                .Count(sc => sc.CourseID == courseID && sc.ScheduleID == scheduleID)
                != 0;
        }

        public void RemoveCourseFromSchedule(Guid courseID, Guid scheduleID)
        {
            ScheduleCourse sc = db.Schedule_Courses
                .Where(r => r.CourseID == courseID && r.ScheduleID == scheduleID)
                .FirstOrDefault();

            if (sc != null)
            {
                db.Schedule_Courses.Remove(sc);
                db.SaveChanges();
            }
        }

        public void SaveAcceptDenyCount(AcceptDenyCount acceptDenyCount)
        {
            if (acceptDenyCount.UID == Guid.Empty)
                acceptDenyCount.UID = Guid.NewGuid();
            AcceptDenyCount existing = db.AcceptDeny_Count.Find(acceptDenyCount.UID);

            if (existing != null)
            {
                db.Entry(existing).CurrentValues.SetValues(acceptDenyCount);
                db.Entry(existing).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                db.AcceptDeny_Count.Add(acceptDenyCount);
                db.SaveChanges();
            }
        }
    

        public void SaveSchedule(Schedule schedule)
        {
            if (schedule.ID == Guid.Empty)
                schedule.ID = Guid.NewGuid();
            Schedule existing = db.Schedule.Find(schedule.ID);

            if (existing != null)
            {
                db.Entry(existing).CurrentValues.SetValues(schedule);
                db.Entry(existing).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                db.Schedule.Add(schedule);
                db.SaveChanges();
            }
        }

        public void Dispose()
        {
            this.authDb.Dispose();
            this.db.Dispose();
        }
    }
}