//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MeetUp.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AcceptDenyCount
    {
        public System.Guid UID { get; set; }
        public bool Accepted { get; set; }
        public int DenyCount { get; set; }
        public System.Guid User1 { get; set; }
        public System.Guid User2 { get; set; }
    
        public virtual User Users { get; set; }
        public virtual User Users1 { get; set; }
    }
}
