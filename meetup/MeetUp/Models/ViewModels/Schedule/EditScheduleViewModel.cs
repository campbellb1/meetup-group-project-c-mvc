﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetUp.Models.ViewModels.Schedule
{
    using Models;

    public class EditScheduleViewModel
    {
        // Data provided by controller
        // ===========================

        public Schedule Schedule { get; set; }
        public Course[] Courses { get; set; }
    }
}