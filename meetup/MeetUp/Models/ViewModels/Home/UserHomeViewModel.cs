﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetUp.Models.ViewModels.Home
{
    using Models;

    public class UserHomeViewModel
    {
        // Data provided by controller
        // ===========================
        //Whether or not the users are "friends"
        public Dictionary<Schedule, bool> Acceptance { get; set; }

        //The user's compatibility with other users.
        public Dictionary<Schedule, double> Compatibility { get; set; }
        // The current user's current schedule.

        public Schedule CurrentSchedule { get; set; }

        // All of the schedules that overlap with the user's current schedule.
        public Schedule[] MatchingSchedules { get; set; }

        // Checks if a user's request is pending.
        public Dictionary<Schedule, bool> Pending { get; set; }

        // A mapping of each schedule in MatchingSchedules to the username of
        // its owner.
        public Dictionary<Schedule, string> UserNames { get; set; }

        // A mapping of each schedule in MatchingSchedules to the courses that
        // it has in common with the user's current schedule.
        public Dictionary<Schedule, Course[]> MatchingCourses { get; set; }
    }
}