﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetUp.Models.ViewModels.Course
{
    using Models;

    public class AddCourseViewModel
    {
        // Data provided by controller
        // ===========================

        public Schedule Schedule { get; set; }
        public IEnumerable<Course> Courses { get; set; }

        // Data posted by view
        // ===================

        public string SubjectCode { get; set; }
        public string CourseNumber { get; set; }
        public string CourseSection { get; set; }
    }
}
