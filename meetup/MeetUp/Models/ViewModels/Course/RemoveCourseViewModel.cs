﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetUp.Models.ViewModels.Course
{
    using Models;

    public class RemoveCourseViewModel
    {
        // Data provided by controller
        // ===========================

        public Schedule Schedule { get; set; }
        public Course Course { get; set; }
    }
}